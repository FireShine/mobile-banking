package ru.fireshine.mobilebanking.exceptions;

public class CardNotActivatedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CardNotActivatedException() {
		super("Карта не активирована");
	}
	
}