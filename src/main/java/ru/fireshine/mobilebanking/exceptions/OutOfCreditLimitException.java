package ru.fireshine.mobilebanking.exceptions;

public class OutOfCreditLimitException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public OutOfCreditLimitException() {
		super("Превышен лимит по кредитной карте");
	}
	
}