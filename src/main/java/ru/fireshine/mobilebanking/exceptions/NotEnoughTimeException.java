package ru.fireshine.mobilebanking.exceptions;

public class NotEnoughTimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NotEnoughTimeException() {
		super("Невозможно выдать кредит меньше, чем за сутки до закрытия кредитной карты");
	}
	
}