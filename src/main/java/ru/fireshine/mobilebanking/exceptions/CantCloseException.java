package ru.fireshine.mobilebanking.exceptions;

public class CantCloseException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CantCloseException() {
		super("Этот вклад нельзя закрыть досрочно");
	}
	
}