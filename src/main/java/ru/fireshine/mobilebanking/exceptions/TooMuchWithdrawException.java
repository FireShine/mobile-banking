package ru.fireshine.mobilebanking.exceptions;

public class TooMuchWithdrawException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public TooMuchWithdrawException() {
		super("Итоговая сумма меньше минимально допустимой");
	}
	
}