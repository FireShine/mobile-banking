package ru.fireshine.mobilebanking.exceptions;

public class NotEnoughMoneyException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NotEnoughMoneyException() {
		super("На счете недостаточно средств");
	}
	
}
