package ru.fireshine.mobilebanking.exceptions;

public class HasExpiredCreditException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public HasExpiredCreditException() {
		super("Есть просроченные кредиты, невозможно выдать новый");
	}
	
}