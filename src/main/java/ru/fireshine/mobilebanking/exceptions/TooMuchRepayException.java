package ru.fireshine.mobilebanking.exceptions;

public class TooMuchRepayException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TooMuchRepayException() {
		super("Внесенная сумма больше необходимой");
	}
	
}