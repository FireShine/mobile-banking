package ru.fireshine.mobilebanking.exceptions;

public class CantWithdrawException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CantWithdrawException() {
		super("С этого вклада нельзя снимать деньги");
	}
	
}