package ru.fireshine.mobilebanking.exceptions;

public class CantReplenishException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CantReplenishException() {
		super("Этот вклад нельзя пополнять");
	}
	
}