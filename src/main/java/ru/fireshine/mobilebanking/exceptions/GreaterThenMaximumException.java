package ru.fireshine.mobilebanking.exceptions;

public class GreaterThenMaximumException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public GreaterThenMaximumException() {
		super("Вы пытаетесь создать вклад на сумму, больше максимально допустимой по тарифу");
	}
	
}