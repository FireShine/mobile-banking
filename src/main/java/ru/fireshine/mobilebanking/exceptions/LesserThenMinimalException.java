package ru.fireshine.mobilebanking.exceptions;

public class LesserThenMinimalException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public LesserThenMinimalException() {
		super("Вы пытаетесь создать вклад на сумму, меньше минимально допустимой по тарифу");
	}
	
}