package ru.fireshine.mobilebanking.exceptions;

public class TooMuchReplenishException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public TooMuchReplenishException() {
		super("Итоговая сумма больше максимально допустимой");
	}
	
}