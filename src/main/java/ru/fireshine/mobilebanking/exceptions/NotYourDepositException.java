package ru.fireshine.mobilebanking.exceptions;

public class NotYourDepositException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NotYourDepositException() {
		super("Вы пытаетесь произвести операцию с чужим вкладом");
	}
	
}