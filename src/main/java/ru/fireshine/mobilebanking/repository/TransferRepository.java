package ru.fireshine.mobilebanking.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ru.fireshine.mobilebanking.entity.Transfer;

public interface TransferRepository extends CrudRepository<Transfer, Long> {
	
	@Query(value = "SELECT t FROM Transfer t WHERE "
			+ "t.from.user.id = :id")
	ArrayList<Transfer> findByUserId(@Param("id") Long id);
	@Query(value = "SELECT t FROM Transfer t WHERE "
			+ "t.status = ru.fireshine.mobilebanking.entity.TransferStatus.SUSPICIOUS")
	ArrayList<Transfer> findSuspicious();
	
}