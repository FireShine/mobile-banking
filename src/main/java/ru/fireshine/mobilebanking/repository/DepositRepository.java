package ru.fireshine.mobilebanking.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ru.fireshine.mobilebanking.entity.Deposit;

public interface DepositRepository extends CrudRepository<Deposit, Long> {
	
	@Query(value = "SELECT d FROM Deposit d WHERE "
			+ "d.user.id = :id")
	ArrayList<Deposit> findByUserId(@Param("id") Long id);
	
}