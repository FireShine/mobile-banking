package ru.fireshine.mobilebanking.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fireshine.mobilebanking.entity.DepositRate;

public interface DepositRateRepository extends CrudRepository<DepositRate, Long> {

}