package ru.fireshine.mobilebanking.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fireshine.mobilebanking.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);
}