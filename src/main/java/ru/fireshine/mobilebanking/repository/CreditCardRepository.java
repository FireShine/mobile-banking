package ru.fireshine.mobilebanking.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ru.fireshine.mobilebanking.entity.CreditCard;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
	@Query(value = "SELECT c FROM CreditCard c WHERE "
			+ "c.status = ru.fireshine.mobilebanking.entity.CreditCardStatus.REQUESTED")
	ArrayList<CreditCard> findRequested();
	@Query(value = "SELECT c FROM CreditCard c WHERE "
			+ "c.status = ru.fireshine.mobilebanking.entity.CreditCardStatus.OFFERED AND "
			+ "c.user.id = :userId")
	ArrayList<CreditCard> findRequestedByUser(@Param("userId") Long userId);
	@Query(value = "SELECT c FROM CreditCard c WHERE "
			+ "c.status = ru.fireshine.mobilebanking.entity.CreditCardStatus.ACTIVATED AND "
			+ "c.user.id = :userId")
	ArrayList<CreditCard> findActiveByUser(@Param("userId") Long userId);
	@Query(value = "SELECT c FROM CreditCard c WHERE "
			+ "c.id = :id AND c.user.id = :userId")
	CreditCard findByIdAndUser(@Param("id") Long id, @Param("userId") Long userId);
}