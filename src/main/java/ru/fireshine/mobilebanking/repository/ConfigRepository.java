package ru.fireshine.mobilebanking.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fireshine.mobilebanking.entity.Config;

public interface ConfigRepository extends CrudRepository<Config, Long> {

}