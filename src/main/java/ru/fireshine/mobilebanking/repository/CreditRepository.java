package ru.fireshine.mobilebanking.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ru.fireshine.mobilebanking.entity.Credit;

public interface CreditRepository extends CrudRepository<Credit, Long> {
	@Query(value = "SELECT c FROM Credit c WHERE "
			+ "c.creditCard.id = :id")
	ArrayList<Credit> findByCreditCard(@Param("id") Long id);
	@Query(value = "SELECT c FROM Credit c WHERE "
			+ "c.creditCard.user.id = :id")
	ArrayList<Credit> findByUserId(@Param("id") Long id);
}