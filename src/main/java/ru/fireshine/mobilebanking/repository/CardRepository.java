package ru.fireshine.mobilebanking.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ru.fireshine.mobilebanking.entity.Card;

public interface CardRepository extends CrudRepository<Card, Long> {
	
	@Query(value = "SELECT c FROM Card c WHERE "
			+ "c.user.id = :id")
	ArrayList<Card> findByUserId(@Param("id") Long id);
	Card findBySerialNumber(Long number);
	
}