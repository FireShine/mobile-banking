package ru.fireshine.mobilebanking.entity;

import javax.persistence.Embeddable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Embeddable
public class FullName {
	
	private String name;
	private String patronymic;
	private String surname;
	
	public FullName(String name, String patronymic, String surname) {
		this.name = name;
		this.patronymic = patronymic;
		this.surname = surname;
	}
	
	@Override
	public String toString() {
		return name + " " + patronymic + " " + surname;
	}
	
}