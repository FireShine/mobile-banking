package ru.fireshine.mobilebanking.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.fireshine.mobilebanking.utils.Messages;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name = "CreditCards")
public class CreditCard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	private User user;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal maxAmount;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal rate;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private Long duration;
	private LocalDate expirationDate;
	@Enumerated(EnumType.STRING)
	private CreditCardStatus status;
	
}