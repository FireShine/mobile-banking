package ru.fireshine.mobilebanking.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.fireshine.mobilebanking.utils.Messages;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name = "DepositRates")
public class DepositRate {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal minAmount;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal maxAmount;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal rate;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private Long duration;
	private boolean capitalized;
	private boolean closable;
	private boolean replenishable;
	private boolean withdrawable;
	
}