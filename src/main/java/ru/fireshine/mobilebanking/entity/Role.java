package ru.fireshine.mobilebanking.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name = "Roles")
public class Role implements GrantedAuthority {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "roles")
    private final Set<User> users = new HashSet<>();
	
	public Role(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
    public void addUser(User user) {
    	users.add(user);
    }
    
	@Override
	public String getAuthority() {
		return getName();
	}
	
}