package ru.fireshine.mobilebanking.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ru.fireshine.mobilebanking.utils.Messages;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "Credits")
public class Credit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	private CreditCard creditCard;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal amount;
	private BigDecimal remain;
	private BigDecimal penny;
	private LocalDate expirationDate;
	
	public Credit() {
		this.remain = BigDecimal.ZERO;
		this.penny = BigDecimal.ZERO;
	}
	
}