package ru.fireshine.mobilebanking.entity;

public enum TransferStatus {
	
	UNCHECKED, SUSPICIOUS, ACCEPTED, REJECTED
	
}