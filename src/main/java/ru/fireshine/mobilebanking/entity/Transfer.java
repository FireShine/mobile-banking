package ru.fireshine.mobilebanking.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ru.fireshine.mobilebanking.utils.Messages;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "Transfers")
public class Transfer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	private Card from;
	@Min(value = 1000_0000_0000_0000L, message = Messages.WRONG_CREDIT_NUMBER)
	private Long destination;
	@Positive(message = Messages.MUST_BE_POSITIVE)
	private BigDecimal amount;
	private LocalDateTime dateTime;
	@Enumerated(EnumType.STRING)
	private TransferStatus status;
	
	public Transfer() {
		this.dateTime = LocalDateTime.now();
		this.status = TransferStatus.UNCHECKED;
	}
	
}