package ru.fireshine.mobilebanking.entity;

public enum CreditCardStatus {
	REQUESTED, OFFERED, ACTIVATED
}