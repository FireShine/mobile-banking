package ru.fireshine.mobilebanking.service;

import java.math.BigDecimal;

public interface CurrencyConverter {
	
	public BigDecimal convert(BigDecimal amount, String from);
	
}