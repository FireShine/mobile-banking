package ru.fireshine.mobilebanking.service;

import java.util.List;
import java.util.Optional;

import ru.fireshine.mobilebanking.entity.CreditCard;

public interface CreditCardService {
	
	public List<CreditCard> findAll();
	public List<CreditCard> findActiveByUser(Long userId);
	public List<CreditCard> findRequestedByUser(Long userId);
	public List<CreditCard> findRequested();
	public Optional<CreditCard> findById(Long id);
	public CreditCard findByIdAndUser(Long id, Long userId);
	public void activate(CreditCard creditCard);
	public void save(CreditCard creditCard);
	public void delete(Long id);
	
}