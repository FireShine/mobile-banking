package ru.fireshine.mobilebanking.service;

import java.util.List;
import java.util.Optional;

import ru.fireshine.mobilebanking.entity.Card;

public interface CardService {
	
	public List<Card> findByUserId(Long id);
	public Optional<Card> findById(Long id);
	
}