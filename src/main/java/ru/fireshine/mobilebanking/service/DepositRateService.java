package ru.fireshine.mobilebanking.service;

import java.util.List;
import java.util.Optional;

import ru.fireshine.mobilebanking.entity.DepositRate;

public interface DepositRateService {
	
	public List<DepositRate> findAll();
	public Optional<DepositRate> findById(Long id);
	public void save(DepositRate depositRate);
	public void delete(Long id);
	
}