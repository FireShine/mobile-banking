package ru.fireshine.mobilebanking.service;

import java.util.List;

import ru.fireshine.mobilebanking.entity.Transfer;

public interface TransferService {
	
	public List<Transfer> findSuspicious();
	public List<Transfer> findByUserId(Long id);
	public List<Transfer> findAll();
	public void transfer(Transfer transfer);
	public void accept(Long id);
	public void save(Transfer transfer);
	public void delete(Long id);
	
}