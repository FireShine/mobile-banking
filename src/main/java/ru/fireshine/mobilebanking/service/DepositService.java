package ru.fireshine.mobilebanking.service;

import java.math.BigDecimal;
import java.util.List;

import ru.fireshine.mobilebanking.entity.Deposit;
import ru.fireshine.mobilebanking.entity.User;

public interface DepositService {
	
	public List<Deposit> findAll();
	public List<Deposit> findByUserId(Long id);
	public void save(Deposit deposit);
	public void withdraw(Long id, BigDecimal amount, User user);
	public void replenish(Long id, BigDecimal amount, User user);
	public void close(Long id, User user);
	public void delete(Deposit deposit);
	
}