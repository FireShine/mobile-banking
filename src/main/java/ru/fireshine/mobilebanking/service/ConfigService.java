package ru.fireshine.mobilebanking.service;

import java.util.List;
import java.util.Optional;

import ru.fireshine.mobilebanking.entity.Config;

public interface ConfigService {
	
	public List<Config> findAll();
	public Optional<Config> findById(Long id);
	public void save(Config config);
	
}