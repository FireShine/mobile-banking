package ru.fireshine.mobilebanking.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lombok.Getter;
import lombok.Setter;
import ru.fireshine.mobilebanking.service.CurrencyConverter;

@Service
public class CurrencyConverterImpl implements CurrencyConverter {
	
	@Getter
	@Setter
	private class Currency {
		
		private String code;
		private BigDecimal nominal;
		private BigDecimal value;
		
	}

	@Override
	public BigDecimal convert(BigDecimal amount, String from) {
		BigDecimal result = BigDecimal.ZERO;
		
		try {
			URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        Document doc = db.parse(new InputSource(url.openStream()));
	        doc.getDocumentElement().normalize();
	        
	        NodeList nodeList = doc.getElementsByTagName("Valute");

	        List<Currency> currencyList = new ArrayList<Currency>();
	        for (int i = 0; i < nodeList.getLength(); i++) {
	            currencyList.add(getCurrency(nodeList.item(i)));
	        }
	        
	        for (Currency currency : currencyList) {
	        	if (currency.getCode().equalsIgnoreCase(from)) {
	        		BigDecimal nominal = currency.getNominal();
	        		BigDecimal value = currency.getValue();
	        		result = amount.multiply(value.divide(nominal, RoundingMode.HALF_UP));
	        		break;
	        	}
	        }
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private Currency getCurrency(Node node) {
		Currency curr = new Currency();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            curr.setCode(getTagValue("CharCode", element));
            curr.setNominal(BigDecimal.valueOf(Integer.parseInt(getTagValue("Nominal", element))));
            String value = getTagValue("Value", element);
            value = value.replace(",", ".");
            curr.setValue(BigDecimal.valueOf(Double.parseDouble(value)));
        }
 
        return curr;
    }

    private String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }
	
}