package ru.fireshine.mobilebanking.service.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.Card;
import ru.fireshine.mobilebanking.entity.Config;
import ru.fireshine.mobilebanking.entity.Transfer;
import ru.fireshine.mobilebanking.entity.TransferStatus;
import ru.fireshine.mobilebanking.exceptions.NotEnoughMoneyException;
import ru.fireshine.mobilebanking.repository.CardRepository;
import ru.fireshine.mobilebanking.repository.ConfigRepository;
import ru.fireshine.mobilebanking.repository.TransferRepository;
import ru.fireshine.mobilebanking.service.TransferService;

@Service
@Transactional
public class TransferServiceImpl implements TransferService {
	
	private static final Integer OUR_BANK_CODE = 427680;
	private final Integer maxTransfers = 5;
	private final Long period = 1L;	//in hours
	private final BigDecimal maxAmount = BigDecimal.valueOf(100_000);
	private final Integer hundredPercent = 2;
	
	private final TransferRepository transferRepository;
	private final CardRepository cardRepository;
	private final ConfigRepository configRepository;
	
	@Autowired
	public TransferServiceImpl(TransferRepository transferRepository,
			CardRepository cardRepository, ConfigRepository configRepository) {
		this.transferRepository = transferRepository;
		this.cardRepository = cardRepository;
		this.configRepository = configRepository;
	}

	@Override
	public List<Transfer> findSuspicious() {
		return transferRepository.findSuspicious();
	}

	@Override
	public List<Transfer> findByUserId(Long id) {
		return transferRepository.findByUserId(id);
	}
	
	@Override
	public List<Transfer> findAll() {
		List<Transfer> transfers = new ArrayList<>();
		transferRepository.findAll().forEach(transfers::add);
		return transfers;
	}
	
	@Override
	public void save(Transfer transfer) {
		transferRepository.save(transfer);
	}
	
	@Override
	public void delete(Long id) {
		transferRepository.deleteById(id);
	}
	
	@Override
	public void accept(Long id) {
		Transfer transfer = transferRepository.findById(id).get();
		transfer.setStatus(TransferStatus.ACCEPTED);
		save(transfer);
		Card fromCard = transfer.getFrom();
		fromCard.setBalance(fromCard.getBalance().subtract(transfer.getAmount()));
		cardRepository.save(fromCard);
	}

	@Override
	public void transfer(Transfer transfer) {
		Long to = transfer.getDestination();
		Card fromCard = transfer.getFrom();
		Config config = configRepository.findById(1L).get();
		if (to / 100_0000_0000L != OUR_BANK_CODE) {
			BigDecimal newAmount = transfer.getAmount()
					.multiply((BigDecimal.ONE
							.add(config.getTransferComission().movePointLeft(hundredPercent))));
			if (fromCard.getBalance().compareTo(newAmount) < 0) {
				throw new NotEnoughMoneyException();
			}
			if (isSuspicious(transfer)) {
				transfer.setStatus(TransferStatus.SUSPICIOUS);
				transfer.setAmount(newAmount);
				save(transfer);
			} else {
				transfer.setStatus(TransferStatus.ACCEPTED);
				transfer.setAmount(newAmount);
				save(transfer);
				fromCard.setBalance(fromCard.getBalance().subtract(transfer.getAmount()));
				cardRepository.save(fromCard);
			}
		} else {
			transfer.setStatus(TransferStatus.ACCEPTED);
			save(transfer);
			Card toCard = cardRepository.findBySerialNumber(transfer.getDestination());
			toCard.setBalance(toCard.getBalance().add(transfer.getAmount()));
			cardRepository.save(toCard);
			fromCard.setBalance(fromCard.getBalance().subtract(transfer.getAmount()));
			cardRepository.save(fromCard);
		}
	}
	
	private boolean isSuspicious(Transfer transfer) {
		boolean suspicious = false;
		Integer transfersNumber = 0;
		for (Transfer tr : findByUserId(transfer.getFrom().getUser().getId())) {
			if (tr.getDateTime().isAfter(LocalDateTime.now().minusHours(period))) {
				transfersNumber++;
			}
		}
		if (transfersNumber > maxTransfers) suspicious = true;
		if (transfer.getAmount().compareTo(maxAmount) > 0) suspicious = true;
		return suspicious;
	}
	
}