package ru.fireshine.mobilebanking.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.Deposit;
import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.exceptions.CantCloseException;
import ru.fireshine.mobilebanking.exceptions.CantReplenishException;
import ru.fireshine.mobilebanking.exceptions.CantWithdrawException;
import ru.fireshine.mobilebanking.exceptions.GreaterThenMaximumException;
import ru.fireshine.mobilebanking.exceptions.LesserThenMinimalException;
import ru.fireshine.mobilebanking.exceptions.NotYourDepositException;
import ru.fireshine.mobilebanking.exceptions.TooMuchReplenishException;
import ru.fireshine.mobilebanking.exceptions.TooMuchWithdrawException;
import ru.fireshine.mobilebanking.repository.DepositRepository;
import ru.fireshine.mobilebanking.service.DepositService;

@Service
@Transactional
public class DepositServiceImpl implements DepositService {
	
	private final DepositRepository depositRepository;
	
	@Autowired
	public DepositServiceImpl(DepositRepository depositRepository) {
		this.depositRepository = depositRepository;
	}

	@Override
	public List<Deposit> findAll() {
		List<Deposit> deposits = new ArrayList<>();
		depositRepository.findAll().forEach(deposits::add);
		return deposits;
	}
	
	@Override
	public List<Deposit> findByUserId(Long id) {
		return depositRepository.findByUserId(id);
	}

	@Override
	public void save(Deposit deposit) {
		if (deposit.getAmount().compareTo(deposit.getDepositRate().getMinAmount()) < 0) {
			throw new LesserThenMinimalException();
		}
		if (deposit.getAmount().compareTo(deposit.getDepositRate().getMaxAmount()) > 0) {
			throw new GreaterThenMaximumException();
		}
		deposit.setMonthMin(deposit.getAmount());
		deposit.setExpirationDate(LocalDate.now().withDayOfMonth(1).plusMonths(deposit.getDepositRate().getDuration()));
		depositRepository.save(deposit);
	}

	@Override
	public void withdraw(Long id, BigDecimal amount, User user) {
		Deposit deposit = depositRepository.findById(id).get();
		if (!user.getId().equals(deposit.getUser().getId())) {
			throw new NotYourDepositException();
		}
		if (!deposit.getDepositRate().isWithdrawable()) {
			throw new CantWithdrawException();
		}
		if (deposit.getAmount().subtract(amount).compareTo(deposit.getDepositRate().getMinAmount()) < 0) {
			throw new TooMuchWithdrawException();
		}
		deposit.setAmount(deposit.getAmount().subtract(amount));
		if (deposit.getAmount().compareTo(deposit.getMonthMin()) < 0) {
			deposit.setMonthMin(deposit.getAmount());
		}
		depositRepository.save(deposit);
	}

	@Override
	public void replenish(Long id, BigDecimal amount, User user) {
		Deposit deposit = depositRepository.findById(id).get();
		if (!user.getId().equals(deposit.getUser().getId())) {
			throw new NotYourDepositException();
		}
		if (!deposit.getDepositRate().isReplenishable()) {
			throw new CantReplenishException();
		}
		if (deposit.getAmount().add(amount).compareTo(deposit.getDepositRate().getMaxAmount()) > 0) {
			throw new TooMuchReplenishException();
		}
		deposit.setAmount(deposit.getAmount().add(amount));
		depositRepository.save(deposit);
	}

	@Override
	public void close(Long id, User user) {
		Deposit deposit = depositRepository.findById(id).get();
		if (!user.getId().equals(deposit.getUser().getId())) {
			throw new NotYourDepositException();
		}
		if (!deposit.getDepositRate().isClosable()) {
			throw new CantCloseException();
		}
		delete(deposit);
	}
	
	@Override
	public void delete(Deposit deposit) {
		depositRepository.delete(deposit);
	}
	
}