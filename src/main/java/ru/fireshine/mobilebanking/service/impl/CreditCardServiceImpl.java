package ru.fireshine.mobilebanking.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.fireshine.mobilebanking.entity.CreditCard;
import ru.fireshine.mobilebanking.entity.CreditCardStatus;
import ru.fireshine.mobilebanking.repository.CreditCardRepository;
import ru.fireshine.mobilebanking.service.CreditCardService;

@Service
@Transactional
public class CreditCardServiceImpl implements CreditCardService {
	
	private final CreditCardRepository creditCardRepository;
	
	@Autowired
	public CreditCardServiceImpl(CreditCardRepository creditCardRepository) {
		this.creditCardRepository = creditCardRepository;
	}

	@Override
	public List<CreditCard> findAll() {
		List<CreditCard> cards = new ArrayList<>();
		creditCardRepository.findAll().forEach(cards::add);
		return cards;
	}
	
	@Override
	public List<CreditCard> findActiveByUser(Long userId) {
		return creditCardRepository.findActiveByUser(userId);
	}

	@Override
	public CreditCard findByIdAndUser(Long id, Long userId) {
		return creditCardRepository.findByIdAndUser(id, userId);
	}

	@Override
	public void save(CreditCard creditCard) {
		creditCardRepository.save(creditCard);
	}

	@Override
	public void delete(Long id) {
		creditCardRepository.deleteById(id);
	}

	@Override
	public List<CreditCard> findRequested() {
		return creditCardRepository.findRequested();
	}

	@Override
	public List<CreditCard> findRequestedByUser(Long userId) {
		return creditCardRepository.findRequestedByUser(userId);
	}

	@Override
	public Optional<CreditCard> findById(Long id) {
		return creditCardRepository.findById(id);
	}

	@Override
	public void activate(CreditCard creditCard) {
		creditCard.setExpirationDate(LocalDate.now().plusDays(creditCard.getDuration()));
		creditCard.setStatus(CreditCardStatus.ACTIVATED);
		save(creditCard);
	}
	
}