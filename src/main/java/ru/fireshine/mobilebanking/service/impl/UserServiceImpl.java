package ru.fireshine.mobilebanking.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.Role;
import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.repository.UserRepository;
import ru.fireshine.mobilebanking.service.UserService;
import ru.fireshine.mobilebanking.utils.Messages;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	private final UserRepository userRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(Messages.USER_NOT_FOUND);
        }
        return user;
	}
	
	@Override
	public boolean saveUser(User user) {
		User dbUser = findByUsername(user.getUsername());

        if (dbUser != null) {
            return false;
        }

        user.addRole(new Role(1L, "ROLE_USER"));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
	}

}