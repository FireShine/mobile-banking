package ru.fireshine.mobilebanking.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.fireshine.mobilebanking.entity.DepositRate;
import ru.fireshine.mobilebanking.repository.DepositRateRepository;
import ru.fireshine.mobilebanking.service.DepositRateService;

@Service
@Transactional
public class DepositRateServiceImpl implements DepositRateService {
	
	private final DepositRateRepository depositRateRepository;
	
	@Autowired
	public DepositRateServiceImpl(DepositRateRepository depositRateRepository) {
		this.depositRateRepository = depositRateRepository;
	}
	
	@Override
	public List<DepositRate> findAll() {
		List<DepositRate> depositRates = new ArrayList<>();
		depositRateRepository.findAll().forEach(depositRates::add);
		return depositRates;
	}

	@Override
	public Optional<DepositRate> findById(Long id) {
		return depositRateRepository.findById(id);
	}

	@Override
	public void save(DepositRate depositRate) {
		depositRateRepository.save(depositRate);
	}

	@Override
	public void delete(Long id) {
		depositRateRepository.deleteById(id);
	}
	
}