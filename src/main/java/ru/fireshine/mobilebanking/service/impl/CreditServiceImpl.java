package ru.fireshine.mobilebanking.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.fireshine.mobilebanking.entity.Credit;
import ru.fireshine.mobilebanking.entity.CreditCard;
import ru.fireshine.mobilebanking.entity.CreditCardStatus;
import ru.fireshine.mobilebanking.exceptions.CardNotActivatedException;
import ru.fireshine.mobilebanking.exceptions.HasExpiredCreditException;
import ru.fireshine.mobilebanking.exceptions.NotEnoughTimeException;
import ru.fireshine.mobilebanking.exceptions.OutOfCreditLimitException;
import ru.fireshine.mobilebanking.exceptions.TooMuchRepayException;
import ru.fireshine.mobilebanking.repository.ConfigRepository;
import ru.fireshine.mobilebanking.repository.CreditRepository;
import ru.fireshine.mobilebanking.service.CreditService;
import ru.fireshine.mobilebanking.service.CurrencyConverter;

@Service
@Transactional
public class CreditServiceImpl implements CreditService {
	
	private final String defaultCurrency = "RUB";
	private final Long minDuration = 1L;
	private final Integer hundredPercent = 2;
	private final Long durability = 30L;
	
	private final CreditRepository creditRepository;
	private final CurrencyConverter currencyConverter;
	private final ConfigRepository configRepository;
	
	@Autowired
	public CreditServiceImpl(CreditRepository creditRepository,
			CurrencyConverter currencyConverter,
			ConfigRepository configRepository) {
		this.creditRepository = creditRepository;
		this.currencyConverter = currencyConverter;
		this.configRepository = configRepository;
	}

	@Override
	public List<Credit> findAll() {
		List<Credit> credits = new ArrayList<>();
		creditRepository.findAll().forEach(credits::add);
		return credits;
	}

	@Override
	public Optional<Credit> findById(Long id) {
		return creditRepository.findById(id);
	}

	@Override
	public void save(Credit credit) {
		creditRepository.save(credit);
	}

	@Override
	public void delete(Long id) {
		creditRepository.deleteById(id);
	}

	@Override
	public List<Credit> findByCreditCard(Long creditCardId) {
		return creditRepository.findByCreditCard(creditCardId);
	}

	@Override
	public List<Credit> findByUserId(Long id) {
		return creditRepository.findByUserId(id);
	}

	@Override
	public void giveCredit(Credit credit, String currency) {
		BigDecimal newAmount;
		BigDecimal sum = BigDecimal.ZERO;
		CreditCard creditCard = credit.getCreditCard();
		if (!creditCard.getStatus().equals(CreditCardStatus.ACTIVATED)) {
			throw new CardNotActivatedException();
		}
		List<Credit> credits = findByUserId(credit.getCreditCard().getUser().getId());
		for (Credit cr : credits) {
			if (cr.getPenny().compareTo(BigDecimal.ZERO) > 0) {
				throw new HasExpiredCreditException();
			}
		}
		if (!LocalDate.now().plusDays(minDuration).isBefore(creditCard.getExpirationDate())) {
			throw new NotEnoughTimeException();
		}
		if (!currency.equals(defaultCurrency)) {
			newAmount = currencyConverter.convert(credit.getAmount(), currency)
					.multiply(BigDecimal.ONE.
							add(configRepository.findById(1L).get().getComission()
									.movePointLeft(hundredPercent)));
			newAmount = newAmount.multiply(BigDecimal.ONE
					.add(creditCard.getRate().movePointLeft(hundredPercent)));
		} else {
			newAmount = credit.getAmount().multiply(BigDecimal.ONE
					.add(creditCard.getRate().movePointLeft(hundredPercent)));
		}
		for (Credit cr : findByCreditCard(creditCard.getId())) {
			sum.add(cr.getAmount());
		}
		if (sum.add(newAmount).compareTo(creditCard.getMaxAmount()) > 0) {
			throw new OutOfCreditLimitException();
		}
		credit.setAmount(newAmount);
		if (!LocalDate.now().plusDays(durability).isBefore(creditCard.getExpirationDate())) {
			credit.setExpirationDate(creditCard.getExpirationDate());
		} else {
			credit.setExpirationDate(LocalDate.now().plusDays(durability));
		}
		save(credit);
	}

	@Override
	public void repayCredit(Long id, BigDecimal amount) {
		Credit credit = findById(id).get();
		if (credit.getAmount().subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
			throw new TooMuchRepayException();
		}
		credit.setAmount(credit.getAmount().subtract(amount));
		if (credit.getAmount().compareTo(BigDecimal.ZERO) == 0) {
			delete(credit.getId());
		} else {
			save(credit);
		}
	}

}
