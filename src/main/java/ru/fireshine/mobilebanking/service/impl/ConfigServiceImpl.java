package ru.fireshine.mobilebanking.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.fireshine.mobilebanking.entity.Config;
import ru.fireshine.mobilebanking.repository.ConfigRepository;
import ru.fireshine.mobilebanking.service.ConfigService;

@Service
@Transactional
public class ConfigServiceImpl implements ConfigService {
	
	private final ConfigRepository configRepository;
	
	@Autowired
	public ConfigServiceImpl(ConfigRepository configRepository) {
		this.configRepository = configRepository;
	}
	
	@Override
	public List<Config> findAll() {
		List<Config> configs = new ArrayList<>();
		configRepository.findAll().forEach(configs::add);
		return configs;
	}
	
	@Override
	public Optional<Config> findById(Long id) {
		return configRepository.findById(id);
	}

	@Override
	public void save(Config config) {
		configRepository.save(config);
	}
	
}