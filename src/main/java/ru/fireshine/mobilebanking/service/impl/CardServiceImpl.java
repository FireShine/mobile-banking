package ru.fireshine.mobilebanking.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.Card;
import ru.fireshine.mobilebanking.repository.CardRepository;
import ru.fireshine.mobilebanking.service.CardService;

@Service
@Transactional
public class CardServiceImpl implements CardService {
	
	private final CardRepository cardRepository;
	
	@Autowired
	public CardServiceImpl(CardRepository cardRepository) {
		this.cardRepository = cardRepository;
	}
	
	@Override
	public List<Card> findByUserId(Long id) {
		return cardRepository.findByUserId(id);
	}

	@Override
	public Optional<Card> findById(Long id) {
		return cardRepository.findById(id);
	}
	
}