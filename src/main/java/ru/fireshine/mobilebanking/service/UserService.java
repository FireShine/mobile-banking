package ru.fireshine.mobilebanking.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import ru.fireshine.mobilebanking.entity.User;

public interface UserService extends UserDetailsService {
	
	public User findByUsername(String username);
	public boolean saveUser(User user);
	
}