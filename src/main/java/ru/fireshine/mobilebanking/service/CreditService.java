package ru.fireshine.mobilebanking.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import ru.fireshine.mobilebanking.entity.Credit;

public interface CreditService {
	
	public List<Credit> findAll();
	public List<Credit> findByCreditCard(Long creditCardId);
	public List<Credit> findByUserId(Long id);
	public void giveCredit(Credit credit, String currency);
	public void repayCredit(Long id, BigDecimal amount);
	public Optional<Credit> findById(Long id);
	public void save(Credit credit);
	public void delete(Long id);
	
}