package ru.fireshine.mobilebanking.task;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.CreditCard;
import ru.fireshine.mobilebanking.service.CreditCardService;
import ru.fireshine.mobilebanking.service.CreditService;

@Service
@EnableScheduling
public class CreditCardTask {
	
	private CreditCardService creditCardService;
	private CreditService creditService;
	
	@Autowired
	public CreditCardTask(CreditCardService creditCardService,
			CreditService creditService) {
		this.creditCardService = creditCardService;
		this.creditService = creditService;
	}
	
	@Scheduled(cron = "0 0 0 * * ?", zone = "Europe/Moscow")
	public void creditCardCloseTask() {
		for (CreditCard card : creditCardService.findAll()) {
			if (LocalDate.now().isAfter(card.getExpirationDate())) {
				if (creditService.findByCreditCard(card.getId()).isEmpty()) {
					creditCardService.delete(card.getId());
				}
			}
		}
	}
	
}