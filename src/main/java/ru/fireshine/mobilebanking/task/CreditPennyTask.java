package ru.fireshine.mobilebanking.task;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.Config;
import ru.fireshine.mobilebanking.entity.Credit;
import ru.fireshine.mobilebanking.repository.ConfigRepository;
import ru.fireshine.mobilebanking.service.CreditService;

@Service
@EnableScheduling
public class CreditPennyTask {
	
	private final Integer houndredPercent = 2;
	private CreditService creditService;
	private ConfigRepository configRepository;
	
	@Autowired
	public CreditPennyTask(CreditService creditService, ConfigRepository configRepository) {
		this.creditService = creditService;
		this.configRepository = configRepository;
	}
	
	@Scheduled(cron = "0 0 0 * * ?", zone = "Europe/Moscow")
	public void creditPennyTask() {
		for (Credit cr : creditService.findAll()) {
			if (LocalDate.now().isAfter(cr.getExpirationDate())) {
				if (cr.getPenny().compareTo(BigDecimal.ZERO) == 0) {
					cr.setRemain(cr.getAmount());
				}
				Config config = configRepository.findById(1L).get();
				if (cr.getPenny().compareTo(config.getMaxPenny()) < 0) {
					cr.setPenny(cr.getPenny().add(config.getPenny()));
					cr.setAmount(cr.getRemain()
							.multiply(BigDecimal.ONE
									.add(config.getPenny().movePointLeft(houndredPercent))));
				}
			}
		}
	}
	
}