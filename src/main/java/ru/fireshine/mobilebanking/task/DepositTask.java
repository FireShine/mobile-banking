package ru.fireshine.mobilebanking.task;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ru.fireshine.mobilebanking.entity.Deposit;
import ru.fireshine.mobilebanking.service.DepositService;

@Service
@EnableScheduling
public class DepositTask {
	
	private final Integer houndredPercent = 2;
	private final BigDecimal monthsInYear = BigDecimal.valueOf(12);
	private DepositService depositService;
	
	@Autowired
	public DepositTask(DepositService depositService) {
		this.depositService = depositService;
	}
	
	@Scheduled(cron = "0 0 0 1 1/1 ?", zone = "Europe/Moscow")
	public void depositTask() {
		for (Deposit d : depositService.findAll()) {
			if (d.getDepositRate().isCapitalized()) {
				d.setAmount(d.getAmount()
						.multiply(BigDecimal.ONE
								.add(d.getDepositRate().getRate()
										.divide(monthsInYear, RoundingMode.HALF_UP)
										.movePointLeft(houndredPercent))));
				depositService.save(d);
			} else {
				d.setAmount(d.getMonthMin()
						.multiply(BigDecimal.ONE
								.add(d.getDepositRate().getRate()
										.divide(monthsInYear, RoundingMode.HALF_UP)
										.movePointLeft(houndredPercent))));
				d.setMonthMin(d.getAmount());
				depositService.save(d);
			}
			if (!d.getExpirationDate().isAfter(LocalDate.now())) {
				if (d.isRenewable()) {
					d.setExpirationDate(LocalDate.now().plusMonths(d.getDepositRate().getDuration()));
				} else {
					depositService.delete(d);
				}
			}
		}
	}
	
}