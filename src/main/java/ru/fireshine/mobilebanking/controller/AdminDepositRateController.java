package ru.fireshine.mobilebanking.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.DepositRate;
import ru.fireshine.mobilebanking.service.DepositRateService;

@Controller
@RequestMapping("/office/admin")
public class AdminDepositRateController {
	
	private final DepositRateService depositRateService;
	
	@Autowired
	public AdminDepositRateController(DepositRateService depositRateService) {
		this.depositRateService = depositRateService;
	}
	
	@GetMapping("/deposit-rates")
	public String viewDepositRates(Model m) {
		List<DepositRate> depositRates = depositRateService.findAll();
		m.addAttribute("depositRates", depositRates);
		return "depositRates";
	}
	
	@GetMapping("/deposit-rates/edit")
	public String depositRateForm(Model m, @RequestParam(required = false) Long id) {
		if (id == null) {
			m.addAttribute("depositRate", new DepositRate());
		} else {
			m.addAttribute("depositRate", depositRateService.findById(id));
		}
		return "addDepositRate";
	}
	
	@PostMapping("/deposit-rates/edit")
	public String saveDepositRate(@ModelAttribute("depositRate") @Valid DepositRate depositRate,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "addDepositRate";
		}
		depositRateService.save(depositRate);
		return "redirect:/office/admin/deposit-rates";
	}
	
	@GetMapping("/deposit-rates/delete")
	public String deleteDepositRate(@RequestParam Long id) {
		depositRateService.delete(id);
		return "redirect:/office/admin/deposit-rates";
	}
	
}