package ru.fireshine.mobilebanking.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.service.UserService;

@Controller
public class RegistrationController {
	
	private final UserService userService;
	
	@Autowired
	public RegistrationController(UserService userService) {
		this.userService = userService;
	}
	
	@GetMapping("/registration")
	public String registration(Model m) {
		m.addAttribute("user", new User());
		return "registration";
	}
	
	@PostMapping("/registration")
	public String addUser(@ModelAttribute("user") @Valid User user, 
			BindingResult bindingResult, Model m) {
		if (bindingResult.hasErrors()) {
            return "registration";
        }
		if (!userService.saveUser(user)){
            m.addAttribute("error", "Пользователь с таким именем уже существует");
            return "registration";
        }
		return "redirect:/";
	}
	
	
}