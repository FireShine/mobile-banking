package ru.fireshine.mobilebanking.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.CreditCard;
import ru.fireshine.mobilebanking.entity.CreditCardStatus;
import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.service.CreditCardService;
import ru.fireshine.mobilebanking.service.UserService;

@Controller
@RequestMapping("/office")
public class CreditCardController {

	private final CreditCardService creditCardService;
	private final UserService userService;

	@Autowired
	public CreditCardController(CreditCardService creditCardService,
			UserService userService) {
		this.creditCardService = creditCardService;
		this.userService = userService;
	}

	@GetMapping("/credit-cards")
	public String viewCreditCards(Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		List<CreditCard> creditCards = creditCardService.findActiveByUser(user.getId());
		m.addAttribute("creditCards", creditCards);
		return "creditCards";
	}
	
	@GetMapping("/credit-cards/add")
	public String newCreditCard(Model m) {
		m.addAttribute("creditCard", new CreditCard());
		return "addCreditCard";
	}

	@PostMapping("/credit-cards/add")
	public String requestCreditCard(@ModelAttribute("creditCard") @Valid CreditCard creditCard,
			BindingResult bindingResult, Principal principal) {
		if (bindingResult.hasErrors()) {
			return "addCreditCard";
		}
		User user = userService.findByUsername(principal.getName());
		creditCard.setUser(user);
		creditCard.setStatus(CreditCardStatus.REQUESTED);
		creditCardService.save(creditCard);
		return "redirect:/office/credit-cards";
	}
	
	@GetMapping("/credit-cards/requests")
	public String viewRequestedCreditCards(Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		List<CreditCard> creditCards = creditCardService.findRequestedByUser(user.getId());
		m.addAttribute("creditCards", creditCards);
		return "requestedCreditCards";
	}
	
	@GetMapping("/credit-cards/requests/accept")
	public String acceptCreditCard(@RequestParam Long id, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		CreditCard creditCard = creditCardService.findByIdAndUser(id, user.getId());
		creditCardService.activate(creditCard);
		return "redirect:/office/credit-cards/requests";
	}

	@GetMapping("/credit-cards/requests/decline")
	public String declineCreditCard(@RequestParam Long id) {
		creditCardService.delete(id);
		return "redirect:/office/credit-cards/requests";
	}
	
}