package ru.fireshine.mobilebanking.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.CreditCard;
import ru.fireshine.mobilebanking.entity.CreditCardStatus;
import ru.fireshine.mobilebanking.service.CreditCardService;

@Controller
@RequestMapping("/office/admin")
public class AdminCreditCardController {
	
	private final CreditCardService creditCardService;

	@Autowired
	public AdminCreditCardController(CreditCardService creditCardService) {
		this.creditCardService = creditCardService;
	}
	
	@GetMapping("/credit-cards")
	public String viewRequestedCreditCards(Model m) {
		List<CreditCard> creditCards = creditCardService.findRequested();
		m.addAttribute("creditCards", creditCards);
		return "adminCreditCards";
	}
	
	@GetMapping("/credit-cards/edit")
	public String editCard(Model m, @RequestParam Long id) {
		m.addAttribute("creditCard", creditCardService.findById(id));
		return "adminEditCreditCard";
	}
	
	@PostMapping("/credit-cards/edit")
	public String offerCreditCard(@ModelAttribute("creditCard") @Valid CreditCard creditCard,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "adminEditCreditCard";
		}
		CreditCard bdCreditCard = creditCardService.findById(creditCard.getId()).get();
		bdCreditCard.setStatus(CreditCardStatus.OFFERED);
		bdCreditCard.setMaxAmount(creditCard.getMaxAmount());
		bdCreditCard.setRate(creditCard.getRate());
		bdCreditCard.setDuration(creditCard.getDuration());
		creditCardService.save(bdCreditCard);
		return "redirect:/office/admin/credit-cards";
	}
	
	@GetMapping("/credit-cards/deny")
	public String denyCreditCard(@RequestParam Long id) {
		creditCardService.delete(id);
		return "redirect:/office/admin/credit-cards";
	}
	
}