package ru.fireshine.mobilebanking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.Transfer;
import ru.fireshine.mobilebanking.service.TransferService;

@Controller
@RequestMapping("/office/admin")
public class AdminTransferController {
	
	private final TransferService transferService;
	
	@Autowired
	public AdminTransferController (TransferService transferService) {
		this.transferService = transferService;
	}
	
	@GetMapping("/transfers")
	public String viewSuspicious(Model m) {
		List<Transfer> transfers = transferService.findSuspicious();
		m.addAttribute("transfers", transfers);
		return "adminTransfers";
	}
	
	@GetMapping("/transfers/accept")
	public String acceptTransfer(@RequestParam Long id) {
		transferService.accept(id);
		return "redirect:/office/admin/transfers";
	}
	
	@GetMapping("/transfers/reject")
	public String rejectTransfer(@RequestParam Long id) {
		transferService.delete(id);
		return "redirect:/office/admin/transfers";
	}
	
}