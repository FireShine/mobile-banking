package ru.fireshine.mobilebanking.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.Deposit;
import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.exceptions.CantCloseException;
import ru.fireshine.mobilebanking.exceptions.CantReplenishException;
import ru.fireshine.mobilebanking.exceptions.CantWithdrawException;
import ru.fireshine.mobilebanking.exceptions.GreaterThenMaximumException;
import ru.fireshine.mobilebanking.exceptions.LesserThenMinimalException;
import ru.fireshine.mobilebanking.exceptions.NotYourDepositException;
import ru.fireshine.mobilebanking.exceptions.TooMuchReplenishException;
import ru.fireshine.mobilebanking.exceptions.TooMuchWithdrawException;
import ru.fireshine.mobilebanking.service.DepositRateService;
import ru.fireshine.mobilebanking.service.DepositService;
import ru.fireshine.mobilebanking.service.UserService;
import ru.fireshine.mobilebanking.utils.Messages;

@Controller
@RequestMapping("/office")
public class DepositController {
	
	private final DepositService depositService;
	private final UserService userService;
	private final DepositRateService depositRateService;
	
	@Autowired
	public DepositController(DepositService depositService, UserService userService,
			DepositRateService depositRateService) {
		this.depositService = depositService;
		this.userService = userService;
		this.depositRateService = depositRateService;
	}
	
	@GetMapping("/deposits")
	public String viewDeposits(Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		List<Deposit> deposits = depositService.findByUserId(user.getId());
		m.addAttribute("deposits", deposits);
		return "deposits";
	}
	
	@GetMapping("/deposits/add")
	public String addDepositForm(Model m) {
		m.addAttribute("depositRates", depositRateService.findAll());
		m.addAttribute("deposit", new Deposit());
		return "addDeposit";
	}
	
	@PostMapping("/deposits/add")
	public String addDeposit(@ModelAttribute("deposit") @Valid Deposit deposit,
			BindingResult bindingResult, Principal principal, Model m, @RequestParam Long rateId) {
		User user = userService.findByUsername(principal.getName());
		if (bindingResult.hasErrors()) {
			m.addAttribute("depositRates", depositRateService.findAll());
			return "addDeposit";
		}
		deposit.setDepositRate(depositRateService.findById(rateId).get());
		deposit.setUser(user);
		try {
			depositService.save(deposit);
			return "redirect:/office/deposits";
		} catch (LesserThenMinimalException|
				GreaterThenMaximumException e) {
			m.addAttribute("error", e.getMessage());
			m.addAttribute("depositRates", depositRateService.findAll());
			return "addDeposit";
		}
	}
	
	@GetMapping("/deposits/withdraw")
	public String withdrawDepositForm(Model m, @RequestParam Long id) {
		m.addAttribute("id", id);
		return "withdrawDeposit";
	}
	
	@PostMapping("/deposits/withdraw")
	public String withdrawDeposit(@RequestParam Long id,
			@RequestParam BigDecimal amount, Principal principal, Model m) {
		User user = userService.findByUsername(principal.getName());
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
			m.addAttribute("error", Messages.MUST_BE_POSITIVE);
			m.addAttribute("id", id);
			return "withdrawDeposit";
		}
		try {
			depositService.withdraw(id, amount, user);
			return "redirect:/office/deposits";
		} catch(NotYourDepositException|
				CantWithdrawException|
				TooMuchWithdrawException e) {
			m.addAttribute("error", e.getMessage());
			m.addAttribute("id", id);
			return "withdrawDeposit";
		}
	}
	
	@GetMapping("/deposits/replenish")
	public String replenishDepositForm(Model m, @RequestParam Long id) {
		m.addAttribute("id", id);
		return "replenishDeposit";
	}
	
	@PostMapping("/deposits/replenish")
	public String replenishDeposit(@RequestParam Long id, @RequestParam BigDecimal amount,
			Principal principal, Model m) {
		User user = userService.findByUsername(principal.getName());
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
			m.addAttribute("error", Messages.MUST_BE_POSITIVE);
			m.addAttribute("id", id);
			return "replenishDeposit";
		}
		try {
			depositService.replenish(id, amount, user);
			return "redirect:/office/deposits";
		} catch(NotYourDepositException|
				CantReplenishException|
				TooMuchReplenishException e) {
			m.addAttribute("error", e.getMessage());
			m.addAttribute("id", id);
			return "replenishDeposit";
		}
	}
	
	@GetMapping("/deposits/close")
	public String closeDeposit(@RequestParam Long id, Principal principal,
			Model m) {
		User user = userService.findByUsername(principal.getName());
		try {
			depositService.close(id, user);
			return "redirect:/office/deposits";
		} catch(NotYourDepositException|
				CantCloseException e) {
			m.addAttribute("error", e.getMessage());
			return "deposits";
		}
	}
	
}