package ru.fireshine.mobilebanking.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.Config;
import ru.fireshine.mobilebanking.service.ConfigService;

@Controller
@RequestMapping("/office/admin")
public class AdminConfigController {
	
	private final ConfigService configService;
	
	@Autowired
	public AdminConfigController(ConfigService configService) {
		this.configService = configService;
	}
	
	@GetMapping("/config")
	public String viewConfig(Model m) {
		List<Config> configs = configService.findAll();
		m.addAttribute("configs", configs);
		return "config";
	}
	
	@GetMapping("/config/edit")
	public String editConfigForm(Model m, @RequestParam(required = false) Long id) {
		if (id == null) {
			m.addAttribute("config", new Config());
		} else {
			m.addAttribute("config", configService.findById(id));
		}
		return "editConfig";
	}
	
	@PostMapping("/config/edit")
	public String editConfig(@ModelAttribute("config") @Valid Config config,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "editConfig";
		}
		configService.save(config);
		return "redirect:/office/admin/config";
	}
	
}