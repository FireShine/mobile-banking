package ru.fireshine.mobilebanking.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ru.fireshine.mobilebanking.entity.Transfer;
import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.exceptions.NotEnoughMoneyException;
import ru.fireshine.mobilebanking.service.CardService;
import ru.fireshine.mobilebanking.service.TransferService;
import ru.fireshine.mobilebanking.service.UserService;

@Controller
@RequestMapping("/office")
public class TransferController {
	
	private final TransferService transferService;
	private final UserService userService;
	private final CardService cardService;
	
	@Autowired
	public TransferController(TransferService transferService,
			UserService userService, CardService cardService) {
		this.userService = userService;
		this.transferService = transferService;
		this.cardService = cardService;
	}
	
	@GetMapping("/transfers")
	public String viewTransfers(Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		List<Transfer> transfers = transferService.findByUserId(user.getId());
		m.addAttribute("transfers", transfers);
		return "transfers";
	}
	
	@GetMapping("/transfers/add")
	public String transferForm(Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		m.addAttribute("cards", cardService.findByUserId(user.getId()));
		m.addAttribute("transfer", new Transfer());
		return "addTransfer";
	}
	
	@PostMapping("/transfers/add")
	public String newTransfer(@ModelAttribute("transfer") @Valid Transfer transfer,
			BindingResult bindingResult, Model m, @ModelAttribute("cardId") Long cardId,
			Principal principal) {
		User user = userService.findByUsername(principal.getName());
		if (bindingResult.hasErrors()) {
			m.addAttribute("cards", cardService.findByUserId(user.getId()));
			return "addTransfer";
		}
		transfer.setFrom(cardService.findById(cardId).get());
		try {
			transferService.transfer(transfer);
		} catch (NotEnoughMoneyException e) {
			m.addAttribute("error", e.getMessage());
			m.addAttribute("cards", cardService.findByUserId(user.getId()));
			return "addTransfer";
		}
		return "redirect:/office/transfers";
	}
	
}