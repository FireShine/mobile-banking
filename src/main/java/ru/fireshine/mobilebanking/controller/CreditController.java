package ru.fireshine.mobilebanking.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.fireshine.mobilebanking.entity.Credit;
import ru.fireshine.mobilebanking.entity.CreditCard;
import ru.fireshine.mobilebanking.entity.User;
import ru.fireshine.mobilebanking.exceptions.CardNotActivatedException;
import ru.fireshine.mobilebanking.exceptions.HasExpiredCreditException;
import ru.fireshine.mobilebanking.exceptions.NotEnoughTimeException;
import ru.fireshine.mobilebanking.exceptions.OutOfCreditLimitException;
import ru.fireshine.mobilebanking.exceptions.TooMuchRepayException;
import ru.fireshine.mobilebanking.service.CreditCardService;
import ru.fireshine.mobilebanking.service.CreditService;
import ru.fireshine.mobilebanking.service.UserService;
import ru.fireshine.mobilebanking.utils.Messages;

@Controller
@RequestMapping("/office")
public class CreditController {
	
	private final CreditCardService creditCardService;
	private final CreditService creditService;
	private final UserService userService;

	@Autowired
	public CreditController(CreditCardService creditCardService,
			UserService userService, CreditService creditService) {
		this.creditCardService = creditCardService;
		this.userService = userService;
		this.creditService = creditService;
	}
	
	@GetMapping("/credits/add")
	public String newCredit(Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		m.addAttribute("credit", new Credit());
		m.addAttribute("creditCards", creditCardService.findActiveByUser(user.getId()));
		return "addCredit";
	}
	
	@PostMapping("/credits/add")
	public String addCredit(@ModelAttribute("credit") @Valid Credit credit,
			BindingResult bindingResult, @ModelAttribute("cardId") Long cardId,
			@ModelAttribute("currency") String currency, Model m, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		if (bindingResult.hasErrors()) {
			m.addAttribute("creditCards", creditCardService.findActiveByUser(user.getId()));
			return "addCredit";
		}
		CreditCard creditCard = creditCardService.findById(cardId).get();
		credit.setCreditCard(creditCard);
		try {
			creditService.giveCredit(credit, currency);
		} catch (CardNotActivatedException|
				HasExpiredCreditException|
				NotEnoughTimeException|
				OutOfCreditLimitException e) {
			m.addAttribute("error", e.getMessage());
			m.addAttribute("creditCards", creditCardService.findActiveByUser(user.getId()));
			return "addCredit";
		}
		return "redirect:/office/credits";
	}
	
	@GetMapping("/credits")
	public String viewCredits(Principal principal, Model m) {
		User user = userService.findByUsername(principal.getName());
		List<Credit> credits = creditService.findByUserId(user.getId());
		m.addAttribute("credits", credits);
		return "credits";
	}
	
	@GetMapping("/credits/repay")
	public String repayCredit(Model m, @RequestParam Long id) {
		m.addAttribute("id", id);
		return "repayCredit";
	}
	
	@PostMapping("/credits/repay")
	public String repayCreditActual(@ModelAttribute("id") Long id,
			@ModelAttribute("amount") BigDecimal amount, Model m) {
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
			m.addAttribute("error", Messages.MUST_BE_POSITIVE);
			m.addAttribute("id", id);
			return "repayCredit";
		}
		try {
			creditService.repayCredit(id, amount);
		} catch(TooMuchRepayException e) {
			m.addAttribute("error", e.getMessage());
			m.addAttribute("id", id);
			return "repayCredit";
		}
		return "redirect:/office/credits";
	}
	
}