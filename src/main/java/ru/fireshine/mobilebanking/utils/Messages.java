package ru.fireshine.mobilebanking.utils;

public class Messages {
	
	public static final String MUST_BE_POSITIVE = "Сумма кредита должна быть больше 0.";
	public static final String USER_NOT_FOUND = "Пользователь с таким именем не найден";
	public static final String WRONG_CREDIT_NUMBER = "Номер карты должен состоять из 16 цифр";
	public static final String MUST_BE_LONGER = "Не меньше 3 символов";
	
}